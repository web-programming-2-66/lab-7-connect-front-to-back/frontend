import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const users = ref<User[]>([])
  const initialUser: User = {
    email: '',
    password: '',
    fullName: '',
    gender: 'male',
    role: ['user']
  }
  const editedUser = ref<User>(JSON.parse(JSON.stringify(initialUser)))

  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    editedUser.value = res.data
    loadingStore.finish()
  }

  async function getUsers() {
    loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }

  async function saveUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    if(!user.id) { 
      // Add new
      console.log('Post ' + JSON.stringify(user))
      const res = await userService.addUser(user)

    } else {          
      // Update user
      console.log('Patch ' + JSON.stringify(user))
      const res = await userService.updateUser(user)

    }
    await getUsers()
    loadingStore.finish()
  }

  async function deleteUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    const res = await userService.deleteUser(user)
    await getUsers()
    loadingStore.finish()
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initialUser))
  }

  return { users, getUser, getUsers, saveUser, deleteUser, editedUser, clearForm }
})
